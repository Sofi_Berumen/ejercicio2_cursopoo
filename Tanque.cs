using System;

namespace Ejercicio2{

    class Tanque{
        private decimal cantidadLitrosCombustible;
        const decimal MAXIMO_LITROS = 40;

        public Tanque(){//CONSTRUCTOR//
            cantidadLitrosCombustible = 0;
        }

        public void vaciarTanque(decimal cantidadCombustible){
            if(cantidadCombustible <= cantidadLitrosCombustible){
                cantidadLitrosCombustible -= cantidadCombustible;
                Console.WriteLine("Total de combustibleVT: " + cantidadLitrosCombustible.ToString());
            }else{
                cantidadLitrosCombustible = 0;
                Console.WriteLine("Tanque vacio!!");
            }
        }

        public void llenarTanques(decimal cantidadCombustible){
            if(cantidadCombustible > 0){
                if((cantidadCombustible + cantidadLitrosCombustible) <= MAXIMO_LITROS){
                    cantidadLitrosCombustible += cantidadCombustible;
                    Console.WriteLine("Total de combustibleLT: " + cantidadLitrosCombustible.ToString());
                }else{
                    cantidadLitrosCombustible = MAXIMO_LITROS;
                    Console.WriteLine("Tanque lleno!!");
                }
            }
        }

        public decimal obtenerLitros(){
            return cantidadLitrosCombustible;
        }


    }

}