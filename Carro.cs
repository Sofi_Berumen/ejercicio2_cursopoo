using System;

namespace Ejercicio2{

    class Carro{
        public string color;
        public string marca;
        public int anyModelo;
        public int numeroPuertas;
        public decimal precio;
        public Tanque miTanque;//LLAMA A LA CLASE//
        public Motor miMotor;//AQUI TAMBIEN//

        public Carro(){//CONSTRUCTOR//
            miMotor = new Motor();
            miTanque = new Tanque();
        }

        public void acelerar(){
            Console.WriteLine("Estoy acelerando");
            miMotor.consumirCombustible(ref miTanque, (decimal)0.4);
            Console.WriteLine("Remanente de combustible: " + miTanque.obtenerLitros().ToString());
        }

        public void frenar(){
            Console.WriteLine("Estoy frenando");
        }

        public void obtenerCaracteristicas(){
            Console.WriteLine("Caracteristicas del auto");
            Console.WriteLine("Color: " + color);
            Console.WriteLine("Marca: " + marca);
            Console.WriteLine("Año del modelo: " + anyModelo.ToString());
            Console.WriteLine("Numero de puertas: " + numeroPuertas.ToString());
            Console.WriteLine("Precio: " + precio.ToString());
            Console.WriteLine("Tipo de carburador: " + miMotor.obtenerTipoCarburador());
            Console.WriteLine("Numero de cilindros: " + miMotor.obtenerNumeroCilindros());
            Console.WriteLine("Tipo de combustible: " + miMotor.TipoCombustible);
        }

    }



}